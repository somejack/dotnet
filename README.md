**1. Install the .NET SDK**

To start building .NET apps you just need to download and install the .NET SDK (Software Development Kit).

```sh
choco install dotnetcore-sdk
```

**2. Create your app**

Open a new command prompt and run the following commands:

```sh
dotnet new console -o myApp
cd myApp
```

The dotnet command creates a new application of type console for you. The -o parameter creates a directory named myApp where your app is stored, and populates it with the required files. The cd myApp command puts you into the newly created app directory.

**3. Run your app**

In your command prompt, run the following command:

```sh
dotnet run
```

Congratulations, you've built and run your first .NET app!
