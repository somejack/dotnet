﻿using System;

namespace dotnet
{
  class Program
  {
    static void Main(string[] args)
    {
      Console.WriteLine("Test begin.");
      var emptyString = "\"";
      if (string.IsNullOrWhiteSpace(emptyString))
      {
        Console.WriteLine("String is empty!");
      }
      else
      {
        Console.WriteLine("String is not empty!");
      }
    }
  }
}
